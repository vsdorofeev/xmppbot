package com.example.xmppbot;

import org.jivesoftware.smack.*;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.sasl.SASLMechanism;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.DNSUtil;
import org.jivesoftware.smack.util.SystemUtil;
import org.jivesoftware.smack.util.dns.DNSResolver;
import org.jivesoftware.smack.util.dns.HostAddress;
import org.jivesoftware.smack.util.dns.SRVRecord;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.stringprep.XmppStringprepException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@SpringBootApplication
public class XmppbotApplication {

	private static final String RESOURCE = "vdbot@";
	private static final String DOMAIN = "jabber.ru";
	private static final String PASSWORD = "Qfg2kfbot";
	private static final String PORT = "5222";

	public static void main(String[] args) throws IOException, InterruptedException, XMPPException, SmackException {
		SpringApplication.run(XmppbotApplication.class, args);


		XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
				.setSecurityMode(ConnectionConfiguration.SecurityMode.required)
				.setXmppDomain("jabber.ru")
				.setHost("www.jabber.ru")
				.setPort(5222)
				.build();

		XMPPTCPConnection con = new XMPPTCPConnection(config);
		con.connect();

		SASLAuthentication.blacklistSASLMechanism("SCRAM-SHA-1");
		SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");
		SASLAuthentication.unBlacklistSASLMechanism("PLAIN");


		con.login("vdbot@jabber.ru", PASSWORD);
		System.out.println(con.isConnected());

		final ChatManager chatManager = ChatManager.getInstanceFor(con);
		chatManager.addIncomingListener(new IncomingChatMessageListener() {
			@Override
			public void newIncomingMessage(EntityBareJid entityBareJid, Message message, Chat chat) {
				System.out.println("Recived new message from " + entityBareJid + " : " + message.getBody());
				sendResponse(entityBareJid, message, chatManager);
			}
		});


		while (true){

		}
	}

	private static void sendResponse(EntityBareJid jid, Message message, ChatManager chatManager){
		Chat chat =  chatManager.chatWith(jid);
		String msg = message.getBody();

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		msg = msg + " " + timestamp.toString();
		Message newMsg = new Message(jid, msg);
		try {
			chat.send(newMsg);
		} catch (SmackException.NotConnectedException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
